# Cerise (formerly Helios Media Player)
A quick and small multimedia player based on GStreamer.

## Compatible File Formats

Can play all formats compatible with GStreamer and its plugins. Common formats include mp3, mp4, mov, ogg, m3u and midi.

Also able to play remote files and streams, including YouTube, Twitch and BBC iPlayer.

## Screenshot

![Video playing via YouTube. Running on Lubuntu 22.04.](http://x-industries.co.uk/wp-content/uploads/2022/06/Screenshot_2022-06-16_15-50-13.png "Downloaded video open in Cerise. Video is You Are Not Where You Think You Are by Kurzgesagt. Running on Lubuntu 22.04.")

Video playing via YouTube. Running on Lubuntu 22.04.

## Installation

### From Source

Dependencies:

- Python 3
- PyQt5
- PyQt5.QtMultimedia
  - Debian/Ubuntu/Mint: `python3-pyqt5.qtmultimedia`
  - openSUSE: `libqt5-qtmultimedia`
  - Others: `qt5-qtmultimedia`
- GStreamer
- GStreamer plugins *(optional, but required for use) - often called gstreamer1.0-plugins-\* or gst-plugins-\**
- yt-dlp

1. Clone the repository with `git clone https://codeberg.org/X-Industries/cerise.git`
2. Install the dependencies
3. Give the appropriate permissions by running `chmod +x INSTALL`
4. Install by running `sudo ./INSTALL`
5. Delete the `cerise` directory

### For Arch Linux/KaOS:

1. Download the package from the Releases tab
2. Download the checksum from the Releases tab and compare with `sha512sum -c --ignore-missing /path/to/file`
3. Run `sudo pacman -U /path/to/file

### For Debian/Ubuntu/Mint:

1. Download the package from the Releases tab
2. Download the checksum from the Releases tab and compare with `sha512sum -c --ignore-missing /path/to/file`
3. Install it with `sudo apt install --install-recommends /path/to/file`

## Known Bugs and Issues

- Opening files from file manager doesn't work yet.

**This is open-source software and I am not a professional. Feel free to open an issue if there's anything else, or submit a pull request if you think you can improve the program yourself.**
